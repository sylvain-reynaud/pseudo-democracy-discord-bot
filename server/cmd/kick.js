/*
 *  Public
 */

const goulagId = "210517644233801739";

module.exports = {
  /* @desc
   * @param
   * @return
   */
  startVote: function(msg) {
    // Get victim
    mem = msg.mentions.members.first();
    console.log("Un vote pour kick " + mem.nickname + " a démarré !");

    // Check if the author of the vote is in the same channel
    if (mem.voiceChannel == msg.member.voiceChannel) {
      channel = mem.voiceChannel;
      total = channel.members.size;

      console.log(`Il y a ${total} membres qui votent.`);
      return {
        'victim': mem,
        'channel': channel,
        'total': total,
        'votes': []
      };
    }
  },

  vote: function(msg, vote) {
    if (!vote.votes.includes(msg.member.nickname)) {
      vote.votes.push(msg.member.nickname);
      if (vote.votes.length / total >= 0.5) {
        console.log(vote.votes.length / total + " : fin du vote, le kick a été accepté :)");
        kick(vote.victim);
        return {
          'votes': []
        };
      } else {
        console.log(vote.votes.length / total);
        return vote;
      }
    } else {
      console.log(msg.member.nickname + ' a déjà voté !');
    }
  }
};

/*
 *  Private
 */
function kick(mem) {
  OriginChannel = mem.voiceChannel;
  console.log("kick de notre ami " + mem.nickname);
  mem.setVoiceChannel(goulagId);
  if (mem.voiceChannel.id != goulagId) {
    mem.setVoiceChannel(goulagId)
    .then(() => console.log(`Moved ${mem.displayName}`))
    .catch(console.error);
  }
  // console.log('Retour à la maison !');
  // mem.setVoiceChannel(OriginChannel);
}
