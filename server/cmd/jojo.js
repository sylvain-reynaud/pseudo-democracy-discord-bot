/*
 *  Public
 */

module.exports = {
  /* @desc
   * @param
   * @return
   */
  jojoRef: function(message) {

    if (message.content.includes("jojo")) {

      /* randomly chooses one of the 4 jojo pictures */
      var img = randomInt(1,4);
      switch (img) {
        case 1: img = "https://vignette.wikia.nocookie.net/steven-universe/images/6/69/Joseph_joestar_niiiice.jpg/revision/latest?cb=20160701221008.jpg"; break;
        case 2: img = "https://vignette.wikia.nocookie.net/jjba/images/e/e2/Jojos_bizarre_adventure_stardust_crusaders-05-jotaro-star_platinum-stand.jpg/revision/latest?cb=20150520074941.jpg"; break;
        case 3: img = "https://pre00.deviantart.net/c1d5/th/pre/i/2016/163/5/b/jonathan_joestar_2_colored_by_diegoku92-da6156n.jpg"; break;
        case 4: img = "https://vignette.wikia.nocookie.net/jjba/images/2/24/JosukeAnime.PNG/revision/latest/scale-to-width-down/347?cb=20171212081940.jpg"; break;
      }

    } else if (message.content.includes("za warudo")) {

      var img = "https://i.pinimg.com/originals/cf/41/14/cf411426d2712fa2bf5be895c459c3ce.gif";
    
    } else if (message.content.includes("dio")) {

      /* randomly chooses on of the 4 dio pictures */
      var img = randomInt(1,3);
      switch (img) {
        case 1: img = "https://i.imgur.com/bcntUzK.jpg"; break;
        case 2: img = "https://i.ytimg.com/vi/1mWfpCIuMM0/hq720.jpg"; break;
        case 3: img = "https://media0.giphy.com/media/Txps6FW6kEXbG/480w_s.jpg"; break;
      }

    }
    
    message.channel.send({files: [img]});

    function randomInt(min, max) {
      return Math.floor(Math.random() * (max - min + 1) ) + min;
    }

  }
};

/*
 *  Private
 */

var var2 = 0; // Rôles importants
